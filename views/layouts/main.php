<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	
	<?php $this->head() ?>
	
     <!--Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>
<?php $this->beginBody() ?>

<div class="brand">My blog</div>
<div class="address-bar">Россия, Кировская область, г.Киров</div>

    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <a class="navbar-brand" href="index.html">Business Casual</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				<?php
					echo Nav::widget([
					'options' => ['class' => 'nav navbar-nav'],
					'items' => [
						['label' => 'Главная', 'url' => ['/site/index']],
						['label' => 'О блоге', 'url' => ['/site/about']],
						['label' => 'Контакты', 'url' => ['/site/contact']],
						!Yii::$app->user->isGuest ?
							['label' => 'Админка', 'items' => [
                                ['label' => 'Категории', 'url' => ['/category']],
                                ['label' => 'Посты', 'url' => ['/post']],
                                ],
							]:'',
						Yii::$app->user->isGuest ?
							['label' => 'Вход', 'url' => ['/site/login']] :
							['label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
						],
					]);
				?>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>



        <div class="container">
		<?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        <?= $content ?>
        </div>

    </div>

    <footer class="footer">
        <div class="container">
			<div class="col-lg-12 text-center">
				<p>Copyright &copy; Мой блог <?= date('Y') ?></p>
			</div>
        </div>
    </footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
