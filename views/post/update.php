<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PostModel */

$this->title = 'Редактирование поста: ' . ' ' . $model->title;
//$this->params['breadcrumbs'][] = ['label' => 'Post Models', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="post-model-update">
<div class="row">
<div class="box">
<div class="col-lg-12">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
</div>
