<?php

use yii\db\Schema;
use yii\db\Migration;

class m150220_143800_blogtables extends Migration
{
    public function up()
    {
    	$this->createTable('blog_category',[
    		'id' => Schema::TYPE_PK,
    		'title' => Schema::TYPE_STRING.' NOT NULL',
    		]);
    	$this->createTable('blog_post',[
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'anons' => Schema::TYPE_TEXT . ' NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER,
            'publish_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
            ]);
    	$this->createIndex('FK_post_category', 'blog_post', 'category_id');
        // $this->addForeignKey(
        //     'FK_post_category', 'blog_post', 'category_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE'
        // );
    }

    public function down()
    {
        $this->dropTable('blog_post');
        $this->dropTable('blog_category');
    }
}
