<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PostModel */

$this->title = 'Создание поста';
//$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-model-create">
<div class="row">
<div class="box">
<div class="col-lg-12">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
</div>
