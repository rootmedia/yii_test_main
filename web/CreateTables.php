<?php

$arr=require_once('../config/db.php');

echo'<pre>';
print_r($arr);
echo'</pre>';

try
{
	$db = new PDO($arr['dsn'], $arr['username'], $arr['password']);

	$db->exec("
		CREATE TABLE `blog_category` (
  			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  			`title` varchar(255)  NOT NULL,
			PRIMARY KEY (`id`)
		);
	");
	
	$db->exec("
		INSERT INTO `blog_category` (`id`, `title`) VALUES
		(1, 'Текст'),
		(2, 'Стих');
	");
	
	$db->exec("
		CREATE TABLE IF NOT EXISTS `blog_post` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`title` varchar(255) DEFAULT NULL,
			`anons` text,
			`content` mediumtext,
			`category_id` int(10) unsigned DEFAULT NULL,
			`publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`),
			KEY `FK_post_category` (`category_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;
	");
	
	$db->exec("
	INSERT INTO `blog_post` (`id`, `title`, `anons`, `content`, `category_id`, `publish_date`) VALUES
		(1, 'Пост первый', 'Анонс для первого поста.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Содержание первого поста.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 1, '2015-02-19 11:35:53'),
		(2, 'Пост второй', 'Анонс для второго поста.\r\nсиноним к слову процедура\r\nлес состоящий из берёз\r\nле корбюзье архитектура\r\nмультфильм российский дед мороз', 'Содеожание второго поста.\r\n\r\nсиноним к слову процедура\r\nлес состоящий из берёз\r\nле корбюзье архитектура\r\nмультфильм российский дед мороз\r\nарийская национальность\r\nпрививки мифы и реальность\r\nкомпактные кусты цветов\r\nкартина из одних кружков\r\nостеопат в зеленограде\r\nи грянул бой полтавский бой\r\nмне было хорошо с тобой\r\nесть ли монархия в канаде\r\nэнергосервисный контракт\r\nнеобъяснимое но факт\r\n\r\n1 июля 2014, вот и лето прошло', 2, '2015-02-20 11:36:00'),
		(3, 'Пост третий', 'Анонс для третьего поста.\r\nчто можно делать из соломы\r\nподелки в сад из желудей\r\nлюбовь ушла пришла симптомы\r\nрассказы про простых людей\r\n', 'Содержание третьего поста.\r\n\r\nчто можно делать из соломы\r\nподелки в сад из желудей\r\nлюбовь ушла пришла симптомы\r\nрассказы про простых людей\r\nигра весёлые колёса\r\nкривая рыночного спроса\r\nкупить зеленый стойкий лак\r\nк чему устойчив портулак\r\nмультфильм про кешу попугая\r\nв бесстыжей наготе лежал\r\nмиит смоленский филиал\r\nстих с добрым утром дорогая\r\nменяю швабру на шабли\r\nчто делать праздники прошли\r\n\r\nянварь 2015, после каникул', 2, '2015-02-19 11:36:06'),
		(4, 'Пост четвертый', 'Анонс для четвертого поста.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Содержание четвертого поста.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 1, '2015-02-19 11:36:10'),
		(5, 'Пятый пост', 'Анонс для пятого поста', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, vitae, distinctio, possimus repudiandae cupiditate ipsum excepturi dicta neque eaque voluptates tempora veniam esse earum sapiente optio deleniti consequuntur eos voluptatem.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, vitae, distinctio, possimus repudiandae cupiditate ipsum excepturi dicta neque eaque voluptates tempora veniam esse earum sapiente optio deleniti consequuntur eos voluptatem.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, vitae, distinctio, possimus repudiandae cupiditate ipsum excepturi dicta neque eaque voluptates tempora veniam esse earum sapiente optio deleniti consequuntur eos voluptatem.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, vitae, distinctio, possimus repudiandae cupiditate ipsum excepturi dicta neque eaque voluptates tempora veniam esse earum sapiente optio deleniti consequuntur eos voluptatem.', 1, '2015-02-20 11:48:16');
	");
	
}
catch(PDOException $e)
{
	die("Error: ".$e->getMessage());
}

?>